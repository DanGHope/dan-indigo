import React from 'react';
import './App.css';
import Product from './product';
import Modal from './Modal';

class App extends React.Component {
  constructor(props) {
    super();
    this.state = {
      isModalOpen: false,
      isLoading: true,
      //Default product to ensure list is displayed incase this api is not working in future
      products: [
        {
          "id": -1,
          "image": "https://www.foodbev.com/wp-content/uploads/2019/03/Hazelnut-Spread-M-and-Ms.jpg",
          "title": "M and Ms",
          "description": "Hazelnut spread M and M's!",
        },
      ]
    }
  }

  componentDidMount(){
    // A Fake product API i found online to streamline the process
    fetch('https://fakestoreapi.com/products')
            .then(res=>res.json())
            .then(json=>this.setState((prevState) => ({
              isLoading: false,
              products: prevState.products.concat(json)
            })))
  }

  openPage = (url) => {
    this.setState({isModalOpen: true});
  }

  closeModal = () => {
    this.setState({isModalOpen: false});
  }

  render() {
    return (
      <div className="App">
        <header className="App__header">
          Dan's Goodie Store
        </header>
        {/* a small loader i snagged off the internet figured id dress it up */}
        {this.state.isLoading ? <div className="loader"/> : (
          <ul className="product-list">
            {this.state.products.map((product) => (
              <Product key={product.id} product={product} openPage={this.openPage} />
            ))}
          </ul>
        )}
        {this.state.isModalOpen ? <Modal closeModal={this.closeModal} /> : null}
      </div>
    );
  }
}

export default App;
