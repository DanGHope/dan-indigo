import React from "react";

const Product = (props) => (
  <li onClick={() => (props.openPage())} className="product">
    <img className="product__image" alt={props.product.title} src={props.product.image} />
    <div className="product__details">
      <h1>{props.product.title}</h1>
      <p>{props.product.description}</p>
    </div>
  </li>
)

export default Product;