import React from "react";

const Modal = (props) => (
  <div className="modal">
    <div className="modal__body">
      <h1>You are about to leave the page!</h1>
      <button type="button" onClick={() => (props.closeModal())}>Cancel</button>
    </div>
    </div>
)

export default Modal;